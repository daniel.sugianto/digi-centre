<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Auth\Events\PasswordReset;

class AuthController extends Controller
{
    CONST HTTP_OK = Response::HTTP_OK;
    CONST HTTP_CREATED = Response::HTTP_CREATED;
    CONST HTTP_UNAUTHORIZED = Response::HTTP_UNAUTHORIZED;
    CONST HTTP_BAD_REQUEST = Response::HTTP_BAD_REQUEST;

    use SendsPasswordResetEmails, ResetsPasswords {
        SendsPasswordResetEmails::broker insteadof ResetsPasswords;
        ResetsPasswords::credentials insteadof SendsPasswordResetEmails;
    }

    public function login(Request $request){
        $credentials = [

            'email' => $request->email,
            'password' => $request->password

        ];

        if( auth()->attempt($credentials) ){
            $user = Auth::user()->load('role')->load('role.activePermissions');
            if($user->role != null){

                $logs = [

                ];

                $user->loginLogs()->Create($logs);

                $token['token'] = $this->get_user_token($user,env('APP_NAME','DiGi-Pusat'));

                $response = self::HTTP_OK;

                return $this->get_http_response( "success", $token, $response );

            }

        }

        $error = "Unauthorized Access";

        $response = self::HTTP_UNAUTHORIZED;

        return $this->get_http_response( "Error", $error, $response );


    }

    public function register(Request $request)
    {
        $messages = [
            'password.regex' => 'Password  must at least 8 characters, Password must contain at least one symbol, one number , one uppercase and one lowercase letter. e.g symbols (! @ # $ % ^ & *).'
        ];
        $validator = Validator::make($request->all(), [

            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' =>
            [
                'required',
                'string',
                'min:8',             // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[@$!%*#?&]/', // must contain a special character
            ],
            'password_confirmation' => 'required|same:password',

        ],$messages);

        if ($validator->fails()) {

            return response()->json([ 'error'=> $validator->errors()->all() ]);

        }

        $data = $request->all();

        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);

        $success['token'] = $this->get_user_token($user,env('APP_NAME','DiGi-Pusat'));

        $success['name'] =  $user->name;

        $response =  self::HTTP_CREATED;

        return $this->get_http_response( "success", $success, $response );

    }

    public function get_current_user()
    {
        $user = Auth::user()->load('role')->load('role.activePermissions')->load('latestLogin');
        $response =  self::HTTP_OK;
        if(!($user->role != null)){
            $user = null;
            $response =  self::HTTP_UNAUTHORIZED;
        }

        return $user ? $this->get_http_response( "success", $user, $response )
                        : $this->get_http_response( "Unauthenticated user", $user, $response );

    }

    public function sendPasswordResetLink(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse()
    {
        $status ="Password reset email sent.";
        $data = null;
        $response = self::HTTP_OK;
        return $this->get_http_response( $status, $data, $response );

    }
    /**
     * Get the response for a failed password reset link.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse()
    {
        $status ="Email could not be sent to this email address.";
        $data = null;
        $response = self::HTTP_BAD_REQUEST;
        return $this->get_http_response( $status, $data, $response );
    }

    /**
     * Handle reset password
     */
    public function callResetPassword(Request $request)
    {
        return $this->reset($request);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);
        $user->save();
        event(new PasswordReset($user));
    }

    /**
     * Get the response for a successful password reset.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse()
    {
        $status ="Password reset successfully.";
        $data = null;
        $response = self::HTTP_OK;
        return $this->get_http_response( $status, $data, $response );
    }
    /**
     * Get the response for a failed password reset.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse()
    {
        $status ="Failed, Invalid Token.";
        $data = null;
        $response = self::HTTP_BAD_REQUEST;
        return $this->get_http_response( $status, $data, $response );
    }

    public function get_http_response( string $status = null, $data = null, $response ){

        return response()->json([

            'status' => $status,
            'data' => $data,

        ], $response);
    }

    public function get_user_token( $user, string $token_name = null ) {

        return $user->createToken($token_name)->accessToken;

    }
}
