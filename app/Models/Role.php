<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasFactory,SoftDeletes;


    protected $fillable = [
        'name'
    ];

    //Relation
    public function activePermissions()
    {
        return $this->belongsToMany(Permission::class,'role_permission','role_id','permission_id')->withPivot(['activated'])->wherePivot('activated', true);
    }
    public function allPermissions()
    {
        return $this->belongsToMany(Permission::class,'role_permission','role_id','permission_id')->withPivot(['activated']);
    }
}
