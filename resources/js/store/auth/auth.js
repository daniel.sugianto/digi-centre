import ApiService from '../../common/api.service';
import JwtService from '../../common/jwt.service';
// import router from '@/router'

const state = {
  errors: null,
  message: null,
  user: {},
  loggedIn: !!JwtService.getToken()
};

const getters = {
  currentUser (state) {
    return state.user;
  },
  isLoggedIn (state) {
    return state.loggedIn;
  }
};

const actions = {
  getCurrentUser (context) {
    return new Promise((resolve, reject) => {
      ApiService.init();
      ApiService.get('user/current').then(
        response => {
          if (response.status == 200) {
            context.commit('setAuth', {
              user: response.data.data
            });
            resolve(response);
          }
        },
        error => {
          context.commit('purgeAuth');
          reject(error);
        }
      );
    });
  },
  login (context, credentials) {
    return new Promise((resolve, reject) => {
      ApiService.init();

      ApiService.post('/login', credentials).then(
        response => {
          if (response.status == 200) {
            context.commit('setAccessToken', response.data.data.token);
            context.commit('setAuth', {
              user: response.data.user,
              role: response.data.role
            });
          }
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  logout (context) {
    context.commit('purgeAuth');
  },
  emailResetPassword (context, credentials) {
    return new Promise((resolve, reject) => {
      ApiService.init();

      ApiService.post('/reset-password', credentials).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  resetPassword (context, credentials) {
    return new Promise((resolve, reject) => {
      ApiService.init();

      ApiService.post('reset/password', credentials).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  }

};

const mutations = {
  setError (state, error) {
    state.errors = error;
  },
  setAuth (state, user) {
    state.loggedIn = true;
    state.user = user;
    state.errors = {};
  },
  setisLoggedIn () {
    state.loggedIn = true;
  },
  setAccessToken (state, data) {
    JwtService.saveToken(data);
  },
  purgeAuth (state) {
    state.loggedIn = false;
    state.errors = {};
    JwtService.destroyToken();
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
