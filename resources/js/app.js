import './bootstrap';
import router from './router';
import Vue from 'vue';
import App from '../layouts/App.vue';
import Buefy from 'buefy';
import './components/validators';
import { ValidationProvider, ValidationObserver } from 'vee-validate';
import store from './store';
import './common/filters.service.js';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faMinus,
  faPlus,
  faHome,
  faArrowUp,
  faArrowDown,
  faAngleLeft,
  faAngleRight,
  faExclamationCircle,
  faExclamationTriangle,
  faEye,
  faEyeSlash,
  faSearch,
  faUser,
  faBars,
  faSignOutAlt,
  faAngleDown,
  faCircle,
  faCalendarDay,
  faTrash,
  faEdit,
  faUpload,
  faPencilAlt,
  faTag,
  faInfo,
  faCheckSquare,
  faWarehouse,
  faCheckCircle
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(
  faMinus,
  faPlus,
  faHome,
  faArrowUp,
  faArrowDown,
  faAngleLeft,
  faAngleRight,
  faExclamationCircle,
  faExclamationTriangle,
  faEye,
  faEyeSlash,
  faSearch,
  faUser,
  faBars,
  faSignOutAlt,
  faAngleDown,
  faCircle,
  faTrash,
  faEdit,
  faCalendarDay,
  faPencilAlt,
  faUpload,
  faTag,
  faInfo,
  faCheckSquare,
  faWarehouse,
  faCheckCircle
);

Vue.component('FontAwesomeIcon', FontAwesomeIcon);
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);

Vue.config.productionTip = false;
Vue.use(Buefy, {
  defaultIconComponent: 'font-awesome-icon',
  defaultIconPack: 'fas'
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
