import { extend } from 'vee-validate'

import {
  email,
  required,
  alpha_spaces,
  numeric,
  alpha_num
} from 'vee-validate/dist/rules'

extend('email', {
  ...email
})

extend('required', {
  ...required
})

extend('alpha_spaces', {
  ...alpha_spaces
})

extend('numeric', {
  ...numeric
})
extend('alpha_num', {
  ...alpha_num
})
extend('password', {
  message:
    '{_field_} must at least 8 characters, Password must contain at least one symbol, one number , one uppercase and one lowercase letter. e.g symbols (! @ # $ % ^ & *).',
  validate: value => {
    const strong_password = new RegExp(
      '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})'
    ) // regex to check our strong password

    return strong_password.test(value) // Test the regex. Test function returns a true or false value.
  }
})

extend("phone", {
  message: "Ini bukan format untuk no telepon",
  validate(value) {
      return /^-?[\d.]+(?:e-?\d+)?$/.test(value);
  }
})