<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'superadmin',
            'email'     => 'superadmin@mailinator.com',
            'password'  => bcrypt('Superadmin123!'),
            'role_id' => 1,
        ]);

        User::create([
            'name'      => 'admin pusat',
            'email'     => 'adminsunyet@mailinator.com',
            'password'  => bcrypt('Admin123!'),
            'role_id' => 2,
        ]);
    }
}
