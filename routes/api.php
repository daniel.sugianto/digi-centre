<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'API\AuthController@login');
Route::post('register', 'API\AuthController@register');
// Send reset password mail
Route::post('reset-password', 'API\AuthController@sendPasswordResetLink');
// handle reset password form process
Route::post('reset/password', 'API\AuthController@callResetPassword');

Route::middleware('auth:api')->group(function(){
    Route::get('user/current', 'API\AuthController@get_current_user');
});
